class CreateCovids < ActiveRecord::Migration[6.0]
  def change
    create_table :covids do |t|
      t.string :name

      t.timestamps
    end
  end
end
