class CreateJoinTableHistoricalClinicalPatientSymptoms < ActiveRecord::Migration[6.0]
  def change
    create_table :historical_clinical_patients_symptoms do |t|
      t.uuid    :historical_clinical_patient_id
      t.integer :symptom_id
    end
  end
end
