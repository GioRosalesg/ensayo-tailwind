class CreateTests < ActiveRecord::Migration[6.0]
  def change
    create_table :tests, id: :uuid do |t|
      t.string  :name,        null: false
      t.date    :start_date,  null: false
      t.date    :finish_date
      t.boolean :multi,       null: false

      t.timestamps
    end
  end
end
