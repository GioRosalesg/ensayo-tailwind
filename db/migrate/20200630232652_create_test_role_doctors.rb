class CreateTestRoleDoctors < ActiveRecord::Migration[6.0]
  def change
    create_table :test_role_doctors do |t|
      t.references :test, null: false, foreign_key: true, type: :uuid
      t.references :doctor, null: false, foreign_key: true, type: :uuid
      t.references :role, null: false, foreign_key: true

      t.timestamps
    end
  end
end
