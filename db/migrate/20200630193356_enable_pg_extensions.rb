class EnablePgExtensions < ActiveRecord::Migration[6.0]
  def change
    enable_extension 'postgis'
    enable_extension 'pgcrypto'
  end
end
