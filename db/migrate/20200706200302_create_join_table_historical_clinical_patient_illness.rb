class CreateJoinTableHistoricalClinicalPatientIllness < ActiveRecord::Migration[6.0]
  def change
    create_table :historical_clinical_patients_illnesses do |t|
      t.uuid    :historical_clinical_patient_id
      t.integer :illness_id
    end
  end
end
