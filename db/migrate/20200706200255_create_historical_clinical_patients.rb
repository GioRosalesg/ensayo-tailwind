class CreateHistoricalClinicalPatients < ActiveRecord::Migration[6.0]
  def change
    create_table :historical_clinical_patients, id: :uuid do |t|
      t.references :patient, null: false, foreign_key: true, type: :uuid
      t.integer :fc
      t.integer :fr
      t.integer :sato2
      t.string :pas
      t.string :pad
      t.float :temp
      t.references :covid, null: false, foreign_key: true
      t.references :treatment, null: false, foreign_key: true
      t.references :destiny, null: false, foreign_key: true
      t.date :destiny_date
      t.references :hosp_motive, null: false, foreign_key: true
      t.integer :esthosp
      t.integer :estuci
      t.references :treatment_adherence, foreign_key: true

      t.timestamps
    end
  end
end
