class CreateHistoricalLaboratoryPatients < ActiveRecord::Migration[6.0]
  def change
    create_table :historical_laboratory_patients, id: :uuid do |t|
      t.references :patient, null: false, foreign_key: true, type: :uuid
      t.string :eritr
      t.string :hb
      t.string :vgm
      t.string :cmhb
      t.string :plaq
      t.string :vpm
      t.string :leuct
      t.string :neutrt
      t.string :neutrsegm
      t.string :neutrb
      t.string :metamiel
      t.string :promiel
      t.string :blast
      t.string :eosin
      t.string :monoc
      t.string :linf
      t.string :gluc
      t.string :bun
      t.string :urea
      t.string :dimer
      t.string :il6
      t.string :tnf
      t.string :ogf

      t.timestamps
    end
  end
end
