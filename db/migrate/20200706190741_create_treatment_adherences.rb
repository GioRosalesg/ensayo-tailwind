class CreateTreatmentAdherences < ActiveRecord::Migration[6.0]
  def change
    create_table :treatment_adherences do |t|
      t.string :name

      t.timestamps
    end
  end
end
