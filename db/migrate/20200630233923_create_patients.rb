class CreatePatients < ActiveRecord::Migration[6.0]
  def change
    create_table :patients, id: :uuid do |t|
      t.string  :name,          null: false
      t.string  :email,         null: false
      t.string  :nss,           null: false
      t.string  :address,       null: false
      t.string  :postal_code,   null: false
      t.string  :phone,         null: false
      t.date    :birthdate,     null: false
      t.string  :contact_phone, null: false
      t.references :job,        null: false, foreign_key: true
      t.integer :persalud,      null: false
      t.integer :gender,        null: false
      t.float   :weight,        null: false
      t.float   :height,        null: false
      t.references :control_group,null: false, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
