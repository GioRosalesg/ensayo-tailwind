class CreateControlGroups < ActiveRecord::Migration[6.0]
  def change
    create_table :control_groups, id: :uuid do |t|
      t.string  :name,          null: false
      t.references :test,       null: false, foreign_key: true, type: :uuid
      t.integer :person_limit,  null: false
      t.integer :margin_diff

      t.timestamps
    end
  end
end
