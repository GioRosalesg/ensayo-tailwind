class CreateHospMotives < ActiveRecord::Migration[6.0]
  def change
    create_table :hosp_motives do |t|
      t.string :name

      t.timestamps
    end
  end
end
