class CreateJoinTableHistoricalClinicalPatientSecondaryEffects < ActiveRecord::Migration[6.0]
  def change
    create_table :historical_clinical_patients_secondary_effects do |t|
      t.uuid    :historical_clinical_patient_id
      t.integer :secondary_effect_id
    end
  end
end
