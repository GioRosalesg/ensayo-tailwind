# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_07_06_200503) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"
  enable_extension "postgis"

  create_table "control_groups", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.uuid "test_id", null: false
    t.integer "person_limit", null: false
    t.integer "margin_diff"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["test_id"], name: "index_control_groups_on_test_id"
  end

  create_table "covids", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "destinies", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "doctors", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "name", null: false
    t.string "ape_pat", null: false
    t.string "ape_mat", null: false
    t.string "ced_prof"
    t.string "cellphone", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_doctors_on_email", unique: true
    t.index ["reset_password_token"], name: "index_doctors_on_reset_password_token", unique: true
  end

  create_table "historical_clinical_patients", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "patient_id", null: false
    t.integer "fc"
    t.integer "fr"
    t.integer "sato2"
    t.string "pas"
    t.string "pad"
    t.float "temp"
    t.bigint "covid_id", null: false
    t.bigint "treatment_id", null: false
    t.bigint "destiny_id", null: false
    t.date "destiny_date"
    t.bigint "hosp_motive_id", null: false
    t.integer "esthosp"
    t.integer "estuci"
    t.bigint "treatment_adherence_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["covid_id"], name: "index_historical_clinical_patients_on_covid_id"
    t.index ["destiny_id"], name: "index_historical_clinical_patients_on_destiny_id"
    t.index ["hosp_motive_id"], name: "index_historical_clinical_patients_on_hosp_motive_id"
    t.index ["patient_id"], name: "index_historical_clinical_patients_on_patient_id"
    t.index ["treatment_adherence_id"], name: "index_historical_clinical_patients_on_treatment_adherence_id"
    t.index ["treatment_id"], name: "index_historical_clinical_patients_on_treatment_id"
  end

  create_table "historical_clinical_patients_illnesses", force: :cascade do |t|
    t.uuid "historical_clinical_patient_id"
    t.integer "illness_id"
  end

  create_table "historical_clinical_patients_secondary_effects", force: :cascade do |t|
    t.uuid "historical_clinical_patient_id"
    t.integer "secondary_effect_id"
  end

  create_table "historical_clinical_patients_symptoms", force: :cascade do |t|
    t.uuid "historical_clinical_patient_id"
    t.integer "symptom_id"
  end

  create_table "historical_laboratory_patients", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "patient_id", null: false
    t.string "eritr"
    t.string "hb"
    t.string "vgm"
    t.string "cmhb"
    t.string "plaq"
    t.string "vpm"
    t.string "leuct"
    t.string "neutrt"
    t.string "neutrsegm"
    t.string "neutrb"
    t.string "metamiel"
    t.string "promiel"
    t.string "blast"
    t.string "eosin"
    t.string "monoc"
    t.string "linf"
    t.string "gluc"
    t.string "bun"
    t.string "urea"
    t.string "dimer"
    t.string "il6"
    t.string "tnf"
    t.string "ogf"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["patient_id"], name: "index_historical_laboratory_patients_on_patient_id"
  end

  create_table "hosp_motives", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "illnesses", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "jobs", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "patients", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.string "email", null: false
    t.string "nss", null: false
    t.string "address", null: false
    t.string "postal_code", null: false
    t.string "phone", null: false
    t.date "birthdate", null: false
    t.string "contact_phone", null: false
    t.bigint "job_id", null: false
    t.integer "persalud", null: false
    t.integer "gender", null: false
    t.float "weight", null: false
    t.float "height", null: false
    t.uuid "control_group_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["control_group_id"], name: "index_patients_on_control_group_id"
    t.index ["job_id"], name: "index_patients_on_job_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "secondary_effects", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "spatial_ref_sys", primary_key: "srid", id: :integer, default: nil, force: :cascade do |t|
    t.string "auth_name", limit: 256
    t.integer "auth_srid"
    t.string "srtext", limit: 2048
    t.string "proj4text", limit: 2048
  end

  create_table "symptoms", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "test_role_doctors", force: :cascade do |t|
    t.uuid "test_id", null: false
    t.uuid "doctor_id", null: false
    t.bigint "role_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["doctor_id"], name: "index_test_role_doctors_on_doctor_id"
    t.index ["role_id"], name: "index_test_role_doctors_on_role_id"
    t.index ["test_id"], name: "index_test_role_doctors_on_test_id"
  end

  create_table "tests", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.date "start_date", null: false
    t.date "finish_date"
    t.boolean "multi", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "treatment_adherences", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "treatments", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "control_groups", "tests"
  add_foreign_key "historical_clinical_patients", "covids"
  add_foreign_key "historical_clinical_patients", "destinies"
  add_foreign_key "historical_clinical_patients", "hosp_motives"
  add_foreign_key "historical_clinical_patients", "patients"
  add_foreign_key "historical_clinical_patients", "treatment_adherences"
  add_foreign_key "historical_clinical_patients", "treatments"
  add_foreign_key "historical_laboratory_patients", "patients"
  add_foreign_key "patients", "control_groups"
  add_foreign_key "patients", "jobs"
  add_foreign_key "test_role_doctors", "doctors"
  add_foreign_key "test_role_doctors", "roles"
  add_foreign_key "test_role_doctors", "tests"
end
