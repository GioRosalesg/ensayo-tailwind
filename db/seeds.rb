#############################################
#
#    Funciones globales para la migración
#
##############################################
# Función que determina si se debe ejecutar el bloque de
# código recibido, dependiendo si el modelo cuenta con registros
# en base de datos, de lo contrario, muestra un mensaje indicando
# que la tabla ya cuenta con registros.
# Parámetros
#   - model:  Nombre del modelo de ActiveRecord en string.
#   - block:  Bloque de código a ejecutar en caso de estar vacía la tabla.
def execute_seed model
  return puts("No se detecta un bloque de código para ejecutar.") unless block_given?
  unless table_empty?(model)
    yield(model)
    puts "Se han creado #{eval("#{model}.count")} registros en la tabla #{model}."
  else
    puts "La tabla #{model} ya cuenta con valores."
  end
end

# Función que determina si una tabla tiene valores registrados
# Parámetros:
#   - model: Modelo de Rails utilizado para realizar la consulta de registros asociados.
# Retorna:
#   - bool:   Valor booleano que determina si la tabla tiene registros (true) o no (false).
def table_empty? model
  eval("#{model}.count") > 0
end

# Inicia conteo de tiempo de ejecución del seeder
start = Time.now

##########################################################
#
#             Datos fictícios para prueba
#
##########################################################
unless Rails.env.eql? "production"
  # Do stuff here

  # Bloque de código base
  # execute_seed("") do |model|
  #   model.constantize.create!(# attr and values goes here)
  # end
  execute_seed("Job") do |model|
    model.constantize.create!(name: "Médico")
    model.constantize.create!(name: "Académico")
    model.constantize.create!(name: "Albañil")
    model.constantize.create!(name: "Ingeniero")
  end

  execute_seed("Test") do |model|
    model.constantize.create!(name: "Ensayo BUAP", start_date: Date.today, finish_date: (Date.today + 6.months), multi: false)
  end


  execute_seed("Illness") do |model|
    model.constantize.create!(name: "ninguna")
    model.constantize.create!(name: "diabetes")
    model.constantize.create!(name: "obesidad")
    model.constantize.create!(name: "sobrepeso")
    model.constantize.create!(name: "hipertensión")
    model.constantize.create!(name: "enfermedad cardiaca")
    model.constantize.create!(name: "enfermedad pulmonar")
    model.constantize.create!(name: "cáncer")
    model.constantize.create!(name: "inmunosupresión")
    model.constantize.create!(name: "VIH/SIDA")
    model.constantize.create!(name: "enfermedad renal")
    model.constantize.create!(name: "heteropatía")
    model.constantize.create!(name: "alergias respiratorias")
    model.constantize.create!(name: "alergias piel")
    model.constantize.create!(name: "alergias medicamentos")
  end

  execute_seed("Symptom") do |model|
    model.constantize.create!(name: "ninguna")
    model.constantize.create!(name: "congestión nasal")
    model.constantize.create!(name: "respiración rápida")
    model.constantize.create!(name: "opresión en el pecho")
    model.constantize.create!(name: "dolor al respirar")
    model.constantize.create!(name: "tos seca")
    model.constantize.create!(name: "tos con flema")
    model.constantize.create!(name: "alteración del olfato")
    model.constantize.create!(name: "escalofrío")
    model.constantize.create!(name: "dolor de cabeza")
    model.constantize.create!(name: "mareo")
    model.constantize.create!(name: "dolor al respirar")
    model.constantize.create!(name: "confusión")
    model.constantize.create!(name: "vómitos")
    model.constantize.create!(name: "presión baja")
    model.constantize.create!(name: "empeoramiento de enfermedad cardiaca o pulmonar")
    model.constantize.create!(name: "diarrea")
    model.constantize.create!(name: "dolor abdominal")
    model.constantize.create!(name: "alteración de los sabores")
  end

  execute_seed("Covid") do |model|
    model.constantize.create!(name: "negativa")
    model.constantize.create!(name: "positiva")
  end

  execute_seed("Treatment") do |model|
    model.constantize.create!(name: "control")
    model.constantize.create!(name: "interferón")
    model.constantize.create!(name: "interferón + naltrexona")
  end

  execute_seed("Destiny") do |model|
    model.constantize.create!(name: "muerte")
    model.constantize.create!(name: "hospitalización")
    model.constantize.create!(name: "alta por mejoría")
  end

  execute_seed("HospMotive") do |model|
    model.constantize.create!(name: "respiratorio")
    model.constantize.create!(name: "neurológico")
    model.constantize.create!(name: "agravamiento enfermedad previa")
    model.constantize.create!(name: "otro")
  end

  execute_seed("SecondaryEffect") do |model|
    model.constantize.create!(name: "ninguno")
    model.constantize.create!(name: "dermatitis")
    model.constantize.create!(name: "ansiedad")
    model.constantize.create!(name: "insomnio")
    model.constantize.create!(name: "necrosis en sitio de inyección")
    model.constantize.create!(name: "hipersensibilidad")
    model.constantize.create!(name: "dolores osteomusculares")
    model.constantize.create!(name: "alteración en estudios de laboratorio")
  end

  execute_seed("TreatmentAdherence") do |model|
    model.constantize.create!(name: "no se adhirió")
    model.constantize.create!(name: "pobre")
    model.constantize.create!(name: "parcial")
    model.constantize.create!(name: "adecuada")
  end

  execute_seed("ControlGroup") do |model|
    test_id = Test.first.id
    model.constantize.create!(name: "Brazo 1", person_limit: 250, margin_diff: 5, test_id: test_id)
    model.constantize.create!(name: "Brazo 2", person_limit: 250, margin_diff: 5, test_id: test_id)
    model.constantize.create!(name: "Brazo 3", person_limit: 250, margin_diff: 5, test_id: test_id)
  end

  execute_seed("Doctor") do |model|
    model.constantize.create!(email: "prueba@salud.mx", password: "prueba123", name: "alguien", ape_pat: "paterno", ape_mat: "materno", cellphone: "1234567890")
  end
end

########## Fin del bloque de datos de prueba ############

# Termina el conteo del tiempo de ejecución del seeder
puts "El seeder completó su ejecución en #{Time.now.to_f - start.to_f} segundos."
