Rails.application.routes.draw do
  root 'tests#dashboard'
  resources :tests, only: [] do
    get   'patient/:id/created', to: "patients#created", as: :created_patient
    resources :patients,              only: [:new, :create]
  end
  #get 'patients/new'
  #get 'patients/create'
  #get 'patients/created'
  #get 'tests/dashboard'
  # Routes for devise
  # Doctors
  devise_for :doctors
end
