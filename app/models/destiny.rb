class Destiny < ApplicationRecord
  # Associations
  has_many :historical_clinical_patients

  # Validations
  validates :name, presence: true
end
