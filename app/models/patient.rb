class Patient < ApplicationRecord
  # Relations
  belongs_to :job
  belongs_to :control_group
  has_many :historical_clinical_patients,   inverse_of: :patient
  has_many :historical_laboratory_patients, inverse_of: :patient
  accepts_nested_attributes_for :historical_clinical_patients
  accepts_nested_attributes_for :historical_laboratory_patients

  # Enums
  enum gender:    { femenino: 0, masculino: 1 }, _prefix: :genero
  enum persalud:  { no: 0, si: 1 }

  # Virtual variables
  attr_accessor :test_id

  # Validations
  validates :phone, :email, :birthdate, :weight, :height, presence: true
	validates :phone, :contact_phone, uniqueness: true, format: { with: /\d{10}/, message: "No parece ser un número telefónico"}
	validates :email, uniqueness: true, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, message: "No es un correo válido" }
	validates :weight, :height, numericality: {only_integers: true, greater_than: 0}

  # Scopes
  scope :with_age, -> { select("*, date_part('year', age(birthdate))::integer age") }

  #  Callbacks
  before_validation :assign_control_group
  after_create_commit :send_welcome_message
  after_update_commit :send_welcome_message

  # Functions
  def assign_control_group
    if control_group_id
      return
    end

    self.control_group_id    = determine_control_group(test_id)
  end

  private
  def determine_control_group test_id
    patients_by_control_group   = ControlGroup.count_patients_by_test(test_id)
    control_group_margin_diff   = patients_by_control_group.first.margin_diff
    min_patients_on_cg          = patients_by_control_group.last.count

    control_groups_candidates = patients_by_control_group.map{ |cg| cg.id if (cg.count - min_patients_on_cg) < control_group_margin_diff }.compact

    return control_groups_candidates.sample
  end

  def send_welcome_message
    begin
      a = TwilioClient.new
      a.send_whatsapp_text(self.phone, message_text(self.name))
      logger.debug {a.inspect}
    rescue StandardError => e
      logger.debug{e.message}
    end
  end

  def message_text nombre
    "Bienvenido #{nombre}!, por favor, actualiza tu información cada 24 hrs. Saludos!"
  end
end
