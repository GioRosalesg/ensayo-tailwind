class HistoricalClinicalPatient < ApplicationRecord
  # Associations
  belongs_to :patient
  belongs_to :covid
  belongs_to :treatment
  belongs_to :destiny
  belongs_to :hosp_motive
  belongs_to :treatment_adherence
  has_and_belongs_to_many :illnesses
  has_and_belongs_to_many :symptoms
  has_and_belongs_to_many :secondary_effects

  # Validations

  # Callbacks
  before_create :set_default_values_on_relations

  private
  def set_default_values_on_relations
    (self.illness_ids = [1]) unless self.illnesses.any?
    (self.symptom_ids = [1]) unless self.symptoms.any?
    (self.secondary_effect_ids = [1]) unless self.secondary_effects.any?
  end
end
