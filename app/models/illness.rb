class Illness < ApplicationRecord
  # Associations
  has_and_belongs_to_many :historical_clinical_patients

  # Validations
  validates :name, presence: true
end
