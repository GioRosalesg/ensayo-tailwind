class Test < ApplicationRecord
  # Relations
  has_many :control_groups
  has_many :test_role_doctors
  has_many :doctors, through: :text_role_doctors
end
