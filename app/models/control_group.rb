class ControlGroup < ApplicationRecord
  # Associations
  belongs_to  :test
  has_many    :patients

  # Validations
  validates :name, :person_limit, presence: true
  validates :person_limit,  numericality: { only_integer: true, greater_than_or_equal_to: 0}
  validates :margin_diff,   numericality: { only_integer: true, greater_than_or_equal_to: 0}, allow_nil: true

  # Callbacks

  # Scopes
  scope :count_patients_by_test, -> (test_id) { select(:id, "count(patients.id) count", :margin_diff).left_outer_joins(:patients).group(:id).order(count: :desc) }

  # Functions
  def self.random_assign test_id
    cgs = self.where(test_id: test_id)
    count_cg = Patient.control_group_count(cgs.pluck(:id))
    cg_margin = cgs.first.margin_diff
    raise count_cg.inspect
  end
end
