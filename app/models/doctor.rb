class Doctor < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :timeoutable

  # Relations
  has_many :test_role_doctors
  has_many :roles, through: :test_role_doctors

  # Callbacks

  # Validations
  validates :email, :name, :ape_pat, :ape_mat, presence: true
  validates :email, uniqueness: true


  # Instance functions
  def full_name
    [name, ape_pat, ape_mat].join(" ").strip
  end
end
