class TwilioClient
  attr_reader :client

  def initialize
    @client = Twilio::REST::Client.new account_sid, auth_token
  end

  def send_sms_text(user_phone, message)
    @client.messages.create(
      from: sms_phone_number,
      to: "+521#{user_phone}",
      body: message
    )
  end

  def send_whatsapp_text(user_phone, message)
    @client.messages.create(
      from: "whatsapp:#{whatsapp_phone_number}",
      to: "whatsapp:+521#{user_phone}",
      body: message
    )
  end

  private
    def account_sid
      Rails.application.credentials.twilio[:account_sid]
    end

    def auth_token
      Rails.application.credentials.twilio[:auth_token]
    end

    def sms_phone_number
      Rails.application.credentials.twilio[:sms_phone_number]
    end

    def whatsapp_phone_number
      Rails.application.credentials.twilio[:whatsapp_phone_number]
    end
end
