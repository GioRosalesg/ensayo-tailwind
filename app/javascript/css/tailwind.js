module.exports = {
  purge: [],
  theme: {
    container: {
      center: true,
    },
    extend: {
      colors: {
        'brand-orange': '#ff9052',
        'brand-green-light': '#def7f1',
        'brand-green': '#54ddb4',
        'brand-green-dark': '#2a886e',
        'brand-teal': '#2dae8c',
        'brand-gray': '#e3e3e3',
        'brand-gray-dark': '#878787',
        'brand-gray-darker': '#606060',
        'form-green-light': '#a0dfcc',
        'form-green': '#007d99',
      },
      boxShadow: {
        'input': '3px 3px 3px 1px #c3c3c3c3',
        'frontal': '0px 0px 8px 0px rgba(0,0,0,0.75)'
      },
      fontFamily: {
        'body': ['lato']
      }
    },
  },
  variants: {},
  plugins: [],
}
