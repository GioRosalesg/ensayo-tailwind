class PatientsController < ApplicationController
  # Callbacks
  before_action :get_test
  before_action :get_patient_by_phone, only: [:create]
  before_action :get_patient, only: [:created]
  before_action :authenticate_doctor!, only: [:new, :create, :created]

  # Actions
  def new
    @patient = Patient.new
    @patient.historical_clinical_patients.build
    @patient.historical_laboratory_patients.build
  end

  def create
    @patient = Patient.new(patient_params) unless @patient
    @patient.test_id = @test.id

    if @patient.save
      @patient.historical_clinical_patients.create!(historical_clinical_patient_params)
      @patient.historical_laboratory_patients.create!(historical_laboratory_patient_params)
      redirect_to test_created_patient_path(test_id: @test.id, id: @patient.id)
    else
      render :new
    end
  end

  def created
  end

  private
  def get_test
    @test = Test.find(params[:test_id])
  rescue ActiveRecord::RecordNotFound
    flash[:notice] = "Ensayo no encontrado, ¿Está escrito de manera correcta?."
    redirect_to root_path
  end

  def get_patient
    @patient = Patient.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:notice] = "Paciente no encontrado, ¿Está correctamente escrita la URL?."
    redirect_to root_path
  end

  def get_patient_by_phone
    @patient = Patient.find_by(phone: patient_phone_params[:phone])
  end

  def patient_phone_params
    params.require(:patient).permit(:phone)
  end

  def patient_params
    params.require(:patient).permit(:name, :email, :nss, :address, :postal_code, :phone, :birthdate, :contact_phone, :job_id, :persalud, :gender, :weight, :height)
  end

  def historical_clinical_patient_params
    params.require(:patient).
      require(:historical_clinical_patients_attributes).
      require("0").
      permit(:fc, :fr, :sato2, :pas, :pad, :temp, :covid_id, :treatment_id, :destiny_id, :destiny_date, :hosp_motive_id, :esthosp, :estuci, :treatment_adherence_id, secondary_effect_ids: [], illness_ids: [], symptom_ids: [])
  end

  def historical_laboratory_patient_params
    params.require(:patient).require(:historical_laboratory_patients_attributes).require("0").permit(:eritr, :hb, :vgm, :cmhb, :plaq, :vpm, :leuct, :neutrt, :neutrsegm, :neutrb, :metamiel, :promiel, :blast, :eosin, :monoc, :linf, :gluc, :bun, :urea, :dimer, :il6, :tnf, :ogf)
  end
end
