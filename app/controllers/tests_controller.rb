class TestsController < ApplicationController
  before_action :get_first_test
  before_action :authenticate_doctor!

  def dashboard
  end

  private
  def get_first_test
    @test = Test.first
  end
end
